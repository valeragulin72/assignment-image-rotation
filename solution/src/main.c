#include "bmp.h"
#include "file_open_close.h"
#include "file_read_write.h"
#include "rotation.h"
#include <stdio.h>
#include <stdlib.h>


int main( int argc, char** argv ) {
    (void) argc; (void) argv;
    if (argc != 3) {
        perror("problem with arguments");
        return 1;
    }

    FILE* orig_file;
    FILE *rot_file;

    if (open_file(&orig_file, argv[1], "rb") == 1) {
        perror("file opening problem");
        return 1;
    }
    if (open_file(&rot_file, argv[2], "wb") == 1) {
        perror("file opening problem");
        return 1;
    }

    struct bmp_header header;
    struct image orig_image;
    struct image rot_image;
    uint64_t padding;

    if (read_file(orig_file, &header, &orig_image, &padding) == 1) {
        perror("file reading problem");
        return 1;
    }
    image_structure(&orig_image, (int64_t)orig_image.height, (int64_t)orig_image.width);
    rot_image = rotation(&orig_image, &rot_image);

    if (close_file(orig_file) == 1) {
        perror("file closing problem");
        return 1;
    }
    free(orig_image.data);

    if (write_file(rot_file, &header, &rot_image, &padding) == 1) {
        perror("file writing writing");
        return 1;
    }

    if (close_file(rot_file) == 1) {
        perror("file closing problem");
        return 1;
    }
    free(rot_image.data);

    return 0;
}
