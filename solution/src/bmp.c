//
// Created by Валерий Рагулин on 01.11.2022.
//
#include "../include/bmp.h"
#include <stdio.h>
#include <stdlib.h>


struct bmp_header header_creator(struct image* image) {
    return (struct bmp_header) {
            .bfType = 19778,
            .bfileSize = image->height * image->width * sizeof(struct pixel) + image->height * ((4 - ((3 * image->width) % 4)) % 4),
            .bfReserved = 0,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = 40,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = 1,
            .biBitCount = 24,
            .biCompression = 0,
            .biXPelsPerMeter = 0,
            .biYPelsPerMeter = 0,
            .biClrUsed = 0,
            .biClrImportant = 0
    };
}

int read_header(struct bmp_header* header, FILE* file) {
    if (fread(header, sizeof(struct bmp_header), 1, file) != 1) {
        return 1;
    } else {
        return 0;
    }
}

int write_header(struct bmp_header* header, struct image* image, FILE* file) {
    *header = header_creator(image);
    if (fwrite(header, sizeof(struct bmp_header), 1, file) != 1) {
        return 1;
    } else {
        return 0;
    }
}

int from_bmp(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding) {
    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fread(image->data + i*header->biWidth, sizeof(struct bmp_header), 1, file) == 0) {
            return 1;
        }
        if (fseek(file,(long)*padding,SEEK_CUR) != 0) {
            return 1;
        }
    }
    return 0;
}

int to_bmp(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding) {
    *padding = (4 - header->biWidth * sizeof(struct pixel) % 4) % 4;
    const uint8_t nulls[3] = {0};
    for (uint32_t i = 0; i < header->biHeight; i++) {
        if (fwrite(image->data + i * header->biWidth, sizeof(struct pixel), header->biWidth, file) != image->width) {
            return 1;
        }
        if (fwrite(nulls, 1, *padding, file) != *padding) {
            return 1;
        }
    }
    return 0;
}
