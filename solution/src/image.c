//
// Created by Валерий Рагулин on 01.11.2022.
//
#include "../include/image.h"
#include <stdio.h>
#include <stdlib.h>

void image_structure(struct image* image, int64_t width, int64_t height) {
    image->width = width;
    image->height = height;
    image->data = malloc(sizeof(struct pixel) * width * height);
}
