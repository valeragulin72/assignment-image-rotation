//
// Created by Валерий Рагулин on 01.11.2022.
//
#include "../include/file_open_close.h"
#include <stdio.h>

int open_file(FILE** file, char* name, char* type) {
    *file = fopen(name, type);
    if (*file == NULL) {
        return 1;
    } else {
        return 0;
    }
}

int close_file(FILE* file) {
    if (fclose(file)) {
        return 1;
    } else {
        return 0;
    }
}
