//
// Created by Валерий Рагулин on 01.11.2022.
//
#include "../include/file_read_write.h"
#include <stdio.h>

int read_file(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding) {
    *padding = (4 - (int64_t)header->biWidth * sizeof(struct pixel) % 4) % 4;

    if (read_header(header, file) == 1) {
        return 1;
    }
    image_structure(image, header->biWidth, header->biHeight);
    if (from_bmp(file, header, image, padding) == 1) {
        return 1;
    }
    return 0;
}

int write_file(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding) {
    if (write_header(header, image, file) == 1) {
        return 1;
    }
    if (to_bmp(file, header, image, padding) == 1) {
        return 1;
    }
    return 0;
}
