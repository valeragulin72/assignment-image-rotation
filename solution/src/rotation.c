//
// Created by Валерий Рагулин on 01.11.2022.
//
#include "../include/rotation.h"

struct image rotation(struct image* image, struct image* rot_image) {
    for (uint32_t i = 0; i < image->height; i++) {
        for (uint32_t j = 0; j < image->width; j++) {
            rot_image->data[image->height * (image->width - 1 - j) + i] = image->data[image->width * i + j];
        }
    }
    return *rot_image;
}
