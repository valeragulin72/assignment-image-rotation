//
// Created by Валерий Рагулин on 01.11.2022.
//

#ifndef IMAGE_TRANSFORMER_FILE_READ_WRITE_H
#define IMAGE_TRANSFORMER_FILE_READ_WRITE_H

#include "../include/bmp.h"
#include  <stdint.h>
#include <stdio.h>

int read_file(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding);
int write_file(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding);

#endif //IMAGE_TRANSFORMER_FILE_READ_WRITE_H
