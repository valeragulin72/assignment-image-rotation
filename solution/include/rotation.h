//
// Created by Валерий Рагулин on 01.11.2022.
//

#ifndef IMAGE_TRANSFORMER_ROTATION_H
#define IMAGE_TRANSFORMER_ROTATION_H

#include "../include/image.h"
#include  <stdint.h>
#include <stdio.h>

struct image rotation(struct image* image, struct image* rot_image);

#endif //IMAGE_TRANSFORMER_ROTATION_H
