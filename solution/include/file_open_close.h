//
// Created by Валерий Рагулин on 01.11.2022.
//

#ifndef IMAGE_TRANSFORMER_FILE_OPEN_CLOSE_H
#define IMAGE_TRANSFORMER_FILE_OPEN_CLOSE_H

#include <stdio.h>

int open_file(FILE** file, char* name, char* type);
int close_file(FILE* file);

#endif //IMAGE_TRANSFORMER_FILE_OPEN_CLOSE_H
