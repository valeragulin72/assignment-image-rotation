//
// Created by Валерий Рагулин on 01.11.2022.
//

#ifndef IMAGE_TRANSFORMER_BMP_H
#define IMAGE_TRANSFORMER_BMP_H

#include "../include/image.h"
#include  <stdint.h>
#include <stdio.h>


#pragma pack(push, 1)
struct bmp_header
{
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

struct image;

struct bmp_header header_creator(struct image* image);
int read_header(struct bmp_header* header, FILE* file);
int write_header(struct bmp_header* header, struct image* image, FILE* file);
int from_bmp(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding);
int to_bmp(FILE* file, struct bmp_header* header, struct image* image, uint64_t* padding);

#endif //IMAGE_TRANSFORMER_BMP_H
