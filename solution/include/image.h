//
// Created by Валерий Рагулин on 01.11.2022.
//

#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include  <stdint.h>
#include <stdio.h>


struct pixel { uint8_t b, g, r; };
struct image {
    uint64_t width, height;
    struct pixel* data;
};

void image_structure(struct image* image, int64_t width, int64_t height);

#endif //IMAGE_TRANSFORMER_IMAGE_H
